package com.antsoft.mapper;

import com.antsoft.database.BaseMapper;
import com.antsoft.model.TransactionalTest;

/**
 * Created by Jason on 2017/3/2.
 */
public interface TransactionalTestMapper extends BaseMapper<TransactionalTest> {
}
