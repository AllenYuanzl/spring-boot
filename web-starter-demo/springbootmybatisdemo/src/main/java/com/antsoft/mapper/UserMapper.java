package com.antsoft.mapper;

import com.antsoft.database.BaseMapper;
import com.antsoft.model.User;

/**
 * Created by Jason on 2017/3/2.
 */
public interface UserMapper extends BaseMapper<User> {
}
