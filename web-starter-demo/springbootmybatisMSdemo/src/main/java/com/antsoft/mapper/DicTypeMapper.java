package com.antsoft.mapper;

import com.antsoft.database.BaseMapper;
import com.antsoft.model.DicType;

/**
 * Created by Jason on 2017/3/2.
 */
public interface DicTypeMapper extends BaseMapper<DicType> {
}
